def foo(s):
    s = s.split()
    count = int(s[-1])
    s.pop(-1)
    name = " ".join(s)
    return name, count


def main():
    result_dict = {}
    while True:
        string = input()
        if string:
            name, count = foo(string)
            if result_dict.get(name):
                result_dict[name].append(count)
            else:
                result_dict[name] = [count]
        else:
            break
    for name, value in result_dict.items():
        print(f"{name}: {value}; {sum(value)}")


if __name__ == '__main__':
    main()
